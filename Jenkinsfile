pipeline {
    agent { label "agentfarm" }
    environment {
        KEY_FILE = '/home/ubuntu/.ssh/vm-instance-key.pem'
        USER = 'ubuntu'
    }
    stages {
        stage('Delete the workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Installing Ansible') {
          steps {
            script {
	      def exists = fileExists '/usr/bin/ansible'
	      if (exists) {
     	        echo "Skipping Ansbile install - already installed"
              } else {
                sh 'sudo apt update -y'
                sh 'sudo apt install -y wget tree unzip ansible python3-pip python3-apt'
              }
            }
          }
        }
        stage('Download Ansible Code') {
            steps {
                git credentialsId: 'git-repo-creds', url: 'git@bitbucket.org:tt-core/ansible-webserver.git'
            }
        }
        stage('Run absible-lint against playbooks') {
            steps {
                sh 'docker run --rm -v $WORKSPACE/playbooks:/data cytopia/ansible-lint apache-install.yml'
                sh 'docker run --rm -v $WORKSPACE/playbooks:/data cytopia/ansible-lint website-update.yml'
                sh 'docker run --rm -v $WORKSPACE/playbooks:/data cytopia/ansible-lint website-test.yml'
            }
        }
        stage('Send Slack Notification') {
            steps {
                slackSend color:'warning', message: "Please approve ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.JOB_URL} | Open>)"
            }
        }
        stage('Request input') {
            steps {
                input 'Please approve or deny this build'
            }
        }
        stage('Install apache and update website') {
            steps {
                sh 'ansible-playbook -u $USER --private-key $KEY_FILE -i $WORKSPACE/host_inventory $WORKSPACE/playbooks/apache-install.yml'
                sh 'ansible-playbook -u $USER --private-key $KEY_FILE -i $WORKSPACE/host_inventory $WORKSPACE/playbooks/website-update.yml'
            }
        }
        stage('Test website') {
            steps {
                sh 'ansible-playbook -u $USER --private-key $KEY_FILE -i $WORKSPACE/host_inventory $WORKSPACE/playbooks/website-test.yml'
            }
        }
    }
    post {
        success { 
            slackSend color:'warning', message: "Your build suceeded! ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.JOB_URL} | Open>)"
        }
        failure { 
            slackSend color:'warning', message: "Your build failed. ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.JOB_URL} | Open>)"
        }
        
    }
}
